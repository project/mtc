# Multiple Timezone Clock

## INTRODUCTION

  Set multiple clock based on time zone. 

## INSTALLATION:

  1. Install as you would normally install a contributed Drupal module. 
     See: https://www.drupal.org/docs/extending-drupal/installing-modules for further information.

## REQUIREMENTS

  Nothing special is required.

## CONFIGURATION

  1. Install the module "Multiple Timezone Clock".
  2. Admin config Url:- admin/config/multiple_timezone_clock
  3. Fill value under the select cock country fieldset and 
     click to "Add one more" for adding more clocks settings.
  4. Go to the “Block Layout”. Eg:- Admin Menu >> structure >> block layout
  5. Go to your block region where you want to put it.
  6. Click the "Place block" button and in the modal dialog click the 
     "Place block" button next to "Multiple Timezone Clock".
  5. Click on the Save block button.

## UNINSTALLATION

  1. Uninstall as you would normally Uninstall a contributed Drupal module. 
     See: https://www.drupal.org/docs/user_guide/en/config-uninstall.html 
     for further information.
